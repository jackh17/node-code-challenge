# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```

Ans1: The program will log "2" immediately then after 100 milliseconds the program will log 2. As javascript runs asynchronously it does not need to to wait for the first console log to be completed before completing the second.

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

Ans2: The program will log numbers 0 - 10 consecutively. The first time the function is called it console logs the argument given to it - which is 0. it also calls itself recursively which means it will log the new argument provided - d + 1 which is one and then call itself again with the new argument. It will continue to do this until the if condition provided - d is less than ten - is not met. 

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

Ans3: The or operator will return the first operand if it is true. So in this case if an empty string was passed to the function it would operate as expected and an empty string evaluates as false and will therefore return the default value 5. But the issue is if, as an example 0 is passed to the function. 0 also evaluates as false, so although 0 could be a valid input to the function it would still output 5.

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

Ans4: When the variable bar is declared it calls the function foo with its only argument set to 1. The funtion foo returns another function that also takes one parameter. If we were to log the variable bar at this point we would see that it has been defined as the function that foo returns with the 'a' parameter set to one - as that the argument it was intial called with when bar was first declared. 

```js
    function bar(b){
      return 1 + b
    }
```
so the function bar now returns whatever argument it is provided plus one.

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

Ans5: In this function the second parameter provided - 'done' - is a placeholder for a callback function which in this case will execute after 100 milliseconds. 

as what we are passing is just a function definition, the function that has recieved the definition as an argument - in this case the "double" double - can then go on to execute that function at any time.

as an example if we called double with a function that conatined a console.log it would log the first argument multiplied by two after the alotted time specified by the second argument provided to the setTimeout function. 

```js
    function printDouble(x){
      console.log(x)
    }

    double(4, printDouble)
```
