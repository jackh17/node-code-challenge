const express = require('express');
const path = require('path');
const app = express();

const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./location.db', sqlite3.OPEN_READONLY , (err) => {

    if(err){
        console.log(err.message)
    }
    console.log('connected to location database')
})

app.get('/locations', (req, res) => {


    const query = req.query.q

    let sql = `SELECT geonameid, name, latitude, longitude FROM locations WHERE instr("name", ?) ORDER BY name LIMIT 5`;

    db.all(sql, [query], (err, rows) => {

            if(err){
                console.log(err);
            }

            res.send(rows);
        }
    )
});

app.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/public/index.html'));
});

const port = 3000;


app.listen(port, () => {
    console.log(`Server running on port ${port}`)
});
